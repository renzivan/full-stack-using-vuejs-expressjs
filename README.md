# [Work in Progress] Vue.js / Express.js project

## Execute Commands
``` bash
# Start Client
npm run dev

# Start Server
npm run server
```

## Build Setup

``` bash
# install vue-cli
npm i -g vue-cli

# show vue list
vue list
# vue webpack
vue init webpack client


# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run e2e tests
npm run e2e

# run all tests
npm test
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).



### Full Stack Web App using Vue.js & Express.js
- [Part 1 - Intro](https://www.youtube.com/watch?v=Fa4cRMaTDUI)
- [Part 2 - Sequelize](https://www.youtube.com/watch?v=xZMwg5z5VGk)
- [Part 3 - Login](https://www.youtube.com/watch?v=H6hM_5ilhqw)

### What I Learned:
- Setting up a server with Express.js
- Creating API to connect to the server
- Binding input data