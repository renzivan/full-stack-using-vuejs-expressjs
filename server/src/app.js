
const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const morgan = require('morgan')

const app = express()

// Use the log generator called morgan
app.use(morgan('combined'))

// Parse any json request
app.use(bodyParser.json())

// allow any host or client to access this
app.use(cors())

// Endpoint 
app.post('/register', (req, res) => {
    res.send({
        msg: `Registration successful! ${req.body.email}`
    })
})

app.listen(process.env.PORT || 8081)